FROM php:7.4-apache

ARG UID
ARG GID

RUN a2enmod rewrite
RUN a2enmod headers

# Enable https
RUN a2enmod ssl
COPY "./localhost.pem" "/etc/ssl/certs/ssl-cert-snakeoil.pem"
COPY "./localhost-key.pem" "/etc/ssl/private/ssl-cert-snakeoil.key"
RUN cp "/etc/apache2/sites-available/default-ssl.conf" "/etc/apache2/sites-enabled/000-default.conf"

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
    openssl \
    libxml2 \
    coreutils \ 
    libzip-dev \
    libxslt-dev \
    libssl-dev \
    libpng-dev \
    libwebp-dev \
    libjpeg-dev

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN docker-php-ext-configure xmlreader \
    CFLAGS="-I/usr/src/php" 

RUN docker-php-ext-configure gd --with-webp --with-jpeg

RUN docker-php-ext-install -j$(nproc) \
    gd \
    zip \
    xsl \
    phar \
    json \
    iconv \
    mysqli \
    pdo_mysql \
    xmlreader \
    simplexml 

COPY "php.ini" "$PHP_INI_DIR/php.ini"

EXPOSE 80
EXPOSE 443

RUN groupadd -g ${GID} host-user &&\
    useradd -l -u ${UID} -g host-user host-user
RUN usermod -a -G root host-user

RUN chown 1000:1000 "/etc/ssl/certs/ssl-cert-snakeoil.pem"
RUN chown 1000:1000 "/etc/ssl/private/"

RUN mkdir "/tmp/xdebug-profiles"
RUN chown 1000:1000 "/tmp/xdebug-profiles"

USER ${UID}:${GID}
